<?php

namespace App\Providers;

use App\Http\Services\Category\CategoryService;
use App\Http\Services\Category\CategoryServiceInterface;
use Illuminate\Support\ServiceProvider;

class CategoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            CategoryServiceInterface::class,
            CategoryService::class
        );
    }
}
