<?php

namespace App\Providers;

use App\Http\Services\Order\OrderService;
use App\Http\Services\Order\OrderServiceInterface;
use Illuminate\Support\ServiceProvider;

class OrderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            OrderServiceInterface::class,
            OrderService::class
        );
    }
}
