<?php

namespace App\Providers;

use App\Http\Services\Cart\CartService;
use App\Http\Services\Cart\CartServiceInterface;
use Illuminate\Support\ServiceProvider;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            CartServiceInterface::class,
            CartService::class
        );
    }
}
