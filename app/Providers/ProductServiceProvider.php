<?php

namespace App\Providers;

use App\Http\Services\Product\ProductService;
use App\Http\Services\Product\ProductServiceInterface;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            ProductServiceInterface::class,
            ProductService::class
        );
    }
}
